# Junta 2023-2024

Obligatoris per l'associació:

- President: Llorenç (2023)
- Tresorer: Evilham (2023)
- Secretari: Rafael Garcia (2020)

Altres càrrecs:

- Vocalia 1: Victor Oncins (2019)
- Vocalia 2: Aleix Mestres (2023)
- Vocalia 3: Victor Oncins (2023)
- Vocalia 4: Josep Moles (2023)
- Vocalia 5: Ramon Selga (2023)
- Vocalia 6: Pedro Vílchez (2023)
