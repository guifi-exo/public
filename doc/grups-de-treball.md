La millor manera d'avaluar els grups de treball actius és en l'assemblea general ordinària, a data de 2023-3-19:

### exo-servers

Creació: no té data de creació oficial, però el ticket #1 a gitlab quan es va crear el repostiro

Funció: És la comissió tècnica de l'associació

Gestionen les infraestructures crítiques de la eXO (especialment de acer30, i de datacenters): desenvolupaments, integracions, operacions, manteniments, etc. Creat durant [el febrer de 2017 amb la motivació de canviar els equips de acer30](https://gitlab.com/guifi-exo/public/-/blob/master/infrastructure/acer30/hardware.md#as-of-2017-2-21)

Canals:

- Repositori privat de gitlab y ús de sistema de tickets per gestió dels projectes tècnics via https://gitlab.com/guifi-exo/projectes
- Xat via matrix

### exo-processos

Creació: ideat durant la FESC de 2022, primera reunió 2022-10-2 i presentat oficialment com a grup de treball en AGO 2023-3-18

Funció: transparència, documentació de processos i bones pràctiques, difusió metodologia eXO

- Grup de treball exo-processos privat a https://agora.exo.cat/
- Publicació de documents en subcategories d'aquesta categoria https://agora.exo.cat/c/associacio-expansio-de-la-xarxa-oberta/5

### Metasuport

Els socis envien missatges a una direcció d'email que captura el software [request-tracker](https://en.wikipedia.org/wiki/Request_Tracker), reben còpia els membres de metasuport, que gestionen les incidències dels socis i també poden apuntar el número d'hores dedicades. a part d'enviar-nos missatges per a nosaltres comm a comentari, també hi ha un canal de riot per comunicació interna. Creat a inicis del 2019 per la necessitat de manegar de forma més formal la resolució d'incidències amb els socis i ajudar-nos entre els mantenidors, la primera incidència atesa ens va arribar el 2019-2-21

## Grups inactius

### Comunicació

Els socis poc tècnics poden col·laborar a l'associació ajudant en la important tasca comunicativa tal com respondre emails, articles wordpress, twitter, etc.

### Pla de gènere

TODO rafa
