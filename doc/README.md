# Sobre els estatuts

Quan es van generar els [estatuts](./estatuts_exo.odt), es va agafar el model que en aquell
moment existia i es va adaptar als nostres objectius.

A Torre Jussana  està disponible [la documentació bàsica i un model
genèric](http://tjussana.cat/doc/publicacions/GA_7.pdf)
