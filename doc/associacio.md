# Dades de facturació i raó social

* Nom: Associació Expansió de la Xarxa Oberta
* NIF: ESG65319832
* Domicili social: Carrer Hondures 30, 08027 Barcelona (Espai 30)


# Altres dades registrals
L’associació eXO està legalment inscrita a:

* Registre d’Associacions de la Generalitat de Catalunya, Barcelona, Secció 1a, Núm 42364
* Registre d’Operadors de Telecomunicacons sota l’expedient 2011/1440
* Registre d'Operadors d'Internet RIPE
