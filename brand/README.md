# Uso

Usar siempre la versión POSITIVO (la de fondo blanco).

Cuando no quede de otra usar NEGATIVO (la de fondo negro).

Cuando color de fondo no es blanco ni negro usar el svg para exportar el logo sin color de fondo para que se adapte a al fondo necesario. Usar versión POSITIVO o NEGATIVO más conveniente al fondo (preferible POSITIVO).

# Edición

Nunca ponerle recuadro al logo.

Para reducir o ampliar logos partir del .svg, nunca del .jpeg, .png,  del photoshop, del gimp, y otros derivados.

Cualquier modificación se la piden y/o lo consultan con Efraín y las irá añadiendo.

# Impresión y publicación

## Digital

Preferible archivo de vectores (svg)

Como no todos los medios aceptan formato vectores convertirlo a JPG, PNG, etc. siempre al tamaño final

Para pantalla siempre en RGB (color mode)

## Analógico

Para impresión CMYK (color mode)

1. formatos amateurs: tirajes cortos, impresoras caseras, etc. da igual si es jpg, tiff, etc.. lo importante es la calidad y de resolución puede ir a 150 dpi sin problemas

2. formatos profesionales: revistas, folletos en offset, etc. preferible vectores o tiff a 300 dpi. pero casi nunca usamos esta opción
