si un node de trax és offline i hi tenia màquines allà es poden moure de trax, per exemple, quan va caure trax1 es van poder moure les màquines a trax2 de la següent manera:

    root@trax2:~# mv /etc/pve/nodes/trax1/qemu-server/* /etc/pve/nodes/trax2/qemu-server/
