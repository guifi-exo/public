a vegades s'han de deixar els equips d'acer30 en una situació preparada per una aturada total

moure totes les màquines virtuals a discos en storage arxiu (d'aquesta manera també queda un backup allà on estava) - queden connectades doncs via NFS

deixar enceses les màquines necessàries per funcionar en serveis mínims i concentrar-les en trax3. És a dir, tota VM de trax1 i trax2 queda apagada

deixar els HA en ignore

parar volums gluster

    gluster volume stop store
    gluster volume stop vmstore

disable volums gluster gfapi en proxmox

un cop en la recuperació:

    gluster volume start store
    gluster volume start vmstore

verificar que gluster funciona bé

    gluster volume heal vmstore info     # hauria d'haver zeros
    gluster volume heal vmstore info     # hauria d'haver zeros
    gluster peer status
    gluster volume status

activar volums gluster via proxmox: primer gfapi i després gluster

tornar a moure les màquines en storage que estaven (normalment vmstore2 en proxmox)
