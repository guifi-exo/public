[TOC]

## deshabilitar HA

moure els HA a estat ignore ([ref](https://gitlab.com/guifi-exo/projectes/-/issues/468))

```
sed -i 's/started/ignored/g' /etc/pve/ha/resources.cfg
```

## stop all VMs

bulk actions stop machines en els 3 nodes

use [manage_vms.sh](./manage_vms.sh) with action stop

## stop gluster

parar volums gluster

    gluster volume stop store
    gluster volume stop vmstore

systemctl disable gluster

## stop / start physical nodes

apagar cada nodo, cada 2 minutos, siguiendo una secuencia

encender cada uno, cada 2 minutos, siguiendo la secuencia inversa

## start gluster

systemctl enable gluster
systemctl start gluster

selfheal https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/gluster/check_gluster.sh

## start all VMs

use [manage_vms.sh](./manage_vms.sh) with action start

##  tornar a habilitar HA

```
sed -i 's/ignored/started/g' /etc/pve/ha/resources.cfg
```
