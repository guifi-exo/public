situació: són 3 nodes que formen el clúster, però estan ubicats en dos caixes; en una hi ha el trax1 **i** el trax2; en l'altre el trax3. **Quan s'ha de fer un manteniment que afecta trax3 no hi ha problema i no s'ha de prendre cap acció rellevant** ja que el consens amb trax1 i trax2 és suficient; però quan el manteniment és a trax1 o trax2 llavors trax3 es queda sol i pot donar problemes.

El gluster està muntat sobre el muntatge de proxmox, per tant quan no hi ha consens bloqueja el punt de muntatge a només lectura. Per això, **en cas de manteniment en trax1 i/o 2** lo convenient és:

1. moure totes les VMs al NFS (arxiu)

en qemuserver veure quins discos són (normalment és virtio0 però COMPROVAR)

    grep -ir vmstore *

es pot fer amb la interfície web per cada VM i per cada disc

o es pot fer per línia de comandes:

    VMID=xxx
    qm move_disk $VMID virtio0 arxiu
    qm migrate $VMID trax3 --online

2. canviar l'estat dels recursos HA al cluster proxmox de *started* a *ignored*

més informació: *Act as if the service were not managed by HA at all. Useful, when full control over the service is desired temporarily, without removing it from the HA configuration.* src https://pve.proxmox.com/wiki/High_Availability

3. desconnectar correctament el volum gluster *vmstore*, el dels SSDs, i que trax3 no fa servir

així quan torna el clúster amb tots el nodes es pot engegar aquest volum sense haver d'esperar el selfheal (com sí que haurem de fer amb el volum *store*, veure més endavant)

    gluster volume stop vmstore

4. migrar VMs de trax1 i/o trax2 a trax3

5. parar el trax1 i trax2

5. *operacions de manteniment*

si calen operacions de proxmox en les màquines virtuals que han quedat en trax3 per tal d'aconseguir consens en aquesta situació temporal fer servir la comanda:

    pvecm expected 1

> pve will reset quorum itself when it sees the second node up

src https://forum.proxmox.com/threads/cluster-with-2-nodes-no-ha-what-to-do-if-1-node-is-down.18973/

6. engegar trax1 i trax2

    `gluster volume start vmstore`

comprovar que el volum *store* (el dels discos mecànics) no té selfheal pendents i està a 0 (sense tasques pendents de recuperació)

    gluster volume heal store info

7. normalitzar situació

per exemple tornar a posar els discos com estaven
