#!/bin/sh -e

hosts="$(cat <<END
exo-trax3
exo-trax5
exo-trax6
exo-trax7
exo-trax8
END
)"

# stop guarda started vms en fichero
#action="stop"

# start carga started vms de fichero
action="start"

for host in ${hosts}; do
  echo "-------"
  echo "${host}"
  if [ "${action}" = "stop" ]; then
    cmd="qm list | grep running | awk '{ print \$1; }'"
    vms="$(ssh ${host} "${cmd}")"
    mkdir -p "manage_vms"
    cat > manage_vms/${host} <<EOF
${vms}
EOF
  elif [ "${action}" = "start" ]; then
    vms="$(cat manage_vms/${host})"
  fi
  for vm in ${vms}; do
    cmd="qm ${action} ${vm}"

    #########
    # DRY MODE
    #########
    echo ${cmd}
    #########
    # PRODUCTION MODE
    #########
    #ssh ${host} ${cmd}

    sleep 5
  done
done
