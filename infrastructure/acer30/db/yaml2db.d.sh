#!/bin/sh -e

unsafe_stderr() {
  # shellcheck disable=SC2059
  printf -- "$*\n" >&2
}

DEBUG() {
  [ "${DEBUG:-}" ] && unsafe_stderr "$@"
  true
}

jq_q() {
  str="${1}"
  jq_query=".${item}.${str}"
  # DEBUG
  #DEBUG "${jq_query}"
  echo "${datayq}" | jq -r "${jq_query}" | sed 's/null//'
}

process_vlans() {
  datayq="$(echo "${dataf}" | yq '.["vlan_plan"]')"

  # src https://github.com/mikefarah/yq#notice-for-v4x-versions-prior-to-4181

  vlans="$(echo "${datayq}" | jq -r 'keys[]' | grep -vF '-')"

  for item in ${vlans}; do
    vtype="$(jq_q "type")"
    code="$(jq_q "code")"
    subnet4="$(jq_q "subnet")"
    subnet="$(jq_q "subnet6")"
    gateway4="$(jq_q "gateway")"
    gateway="$(jq_q "gateway6")"
    title="$(jq_q "title")"
    notes="$(jq_q "notes")"
    if [ "${title}" ]; then
      sep=' '
    else
      sep=''
    fi
    description="${title}${sep}${notes}"

    vlans_out="${VLANS}/${item}"
    cat > "${vlans_out}" <<EOF
vlan="${item}"
type="${vtype}"
code="${code}"
subnet4="${subnet4}"
gateway4="${gateway4}"
subnet="${subnet}"
gateway="${gateway}"
description="${description}"
$(
if [ "${gateway4}" ] || [ "${gateway}" ]; then
  echo "access='y'"
fi
if [ -z "${subnet4}" ] || [ -z "${subnet}" ]; then
  echo "reserved='y'"
fi
)
EOF
    [ "${DEBUG:-}" ] && cat "${vlans_out}"
    DEBUG "\n-------------\n"
  done
}

process_hosts() {
  scope="${1}"
  #datayq="$(echo "${dataf}" | yq '.["vms_exo","vms_3parties","custom_allocated_servers"] | keys[]')"
  datayq="$(echo "${dataf}" | yq ".[\"${scope}\"]")"

  # src https://github.com/mikefarah/yq#notice-for-v4x-versions-prior-to-4181

  hosts="$(echo "${datayq}" | jq -r 'keys[]' | grep -vF '-')"

  for item in ${hosts}; do
    item="[${item}]"
    hostname="$(jq_q "hostname")"
    DEBUG "___hostname: ${hostname}___\n"
    mkdir -p "${HOSTS}/${hostname}"
    system="$(jq_q "system")"
    vmid="$(jq_q "vmid")"
    ha="$(jq_q "ha")"
    notes="$(jq_q "notes")"
    # src https://stackoverflow.com/questions/1251999/how-can-i-replace-each-newline-n-with-a-space-using-sed
    services="$(jq_q "services" | jq -r '.[]' | sed -z 's/\n/;/g')"
    scope_extra_vars="$(
      # https://stackoverflow.com/questions/34502492/how-to-use-case-esac-in-process-substitution
      # https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_09_04_05
      case "${scope}" in
        (vms_exo)
          echo "vm='y'"
          echo "exovm='y'"
          ;;
        (vms_3parties)
          echo "vm='y'"
          echo "vps='y'"
          ;;
        (custom_allocated_servers)
          echo "cas='y'"
          ;;
      esac
      )"

    hosts_out="${HOSTS}/${hostname}/cfg.env"
    #cat <<EOF
    cat > "${hosts_out}" <<EOF
system="${system}"
vmid="${vmid}"
ha="${ha}"
notes="${notes}"
services="${services}"
${scope_extra_vars}
EOF
    [ "${DEBUG:-}" ] && cat "${hosts_out}"
    #DEBUG "\n-------------------\n"
    DEBUG "\n"

    # vlans
    #   ip
    #   ip6
    # decision: discard iface info, this is going to be part of another file related to proxmox
    # TODO care about comments (include pending operations and reserved stuff)
    # TODO care about TODOs
    # TODO care about routers such as VM bng, guifi-comm1 that are a little bit out of schema
    vlans="$(echo "${datayq}" | jq -r '.'"${item}"' | keys[]' | grep vlan)" || {
      echo "WARNING: host ${hostname} ${vmid} has no vlans (?)"
      sleep 2
    }
    for vlan in ${vlans}; do
      DEBUG "____${vlan}____\n"
      # removes cidr on IPs
      ip="$(jq_q "${vlan}" | jq -r '.["ip"]' | sed 's/null//' |  cut -d'/' -f1)"
      ip6="$(jq_q "${vlan}" | jq -r '.["ip6"]' | sed 's/null//' | cut -d'/' -f1)"
      #static_route_guifi="$(jq_q "${vlan}" | jq -r '.["static_route_guifi"]' | sed 's/null//'))"
      DEBUG "${ip} ${ip6}"
      #cat <<EOF
      vhost_out="${HOSTS}/${hostname}/${vlan}"
      cat > "${vhost_out}" <<EOF
ip4="${ip}"
ip="${ip6}"
EOF
    [ "${DEBUG:-}" ] && cat "${vhost_out}"
    DEBUG "\n"

    done
    DEBUG "\n====================\n"

  done

}

# migrates from yaml to db.d-network
#   warning: this creates files and dirs on your operating system
main() {
  srcf='network.yml'
  dataf="$(cat "${srcf}")"

  VLANS='db.d/vlans'
  rm -rf "${VLANS}"
  mkdir -p "${VLANS}"

  process_vlans

  HOSTS='db.d/hosts'
  rm -rf "${HOSTS}"
  mkdir -p "${HOSTS}"

  process_hosts "vms_exo"
  process_hosts "vms_3parties"
  # TODO complex, better manually?
  #process_hosts "custom_allocated_servers"
  # TODO (but maybe is not needed, as it is not needed other stuff right now)
  #    maybe it's better manually
  #  place
  #  lag
  #  mac
  process_hosts "trax_network"
}

main
