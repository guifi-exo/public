#!/bin/bash

hosts=(
  # vm 125 reported; no vlan95
  # vm 126 is vyos (exception)
  # vm 130 reported; no vlan95
  # vm 133 reported; incomplete details
  # vm 134 is vyos (exception)
  # vm 138 reported; incomplete details
  # vm 139 reported; incomplete details
  # vm 141 is windows (exception)
  # vm 143 reported; no vlan95
  # vm 144 reported; no vlan95
  # vm 145 is off(?)
  192.168.95.{21,22,23,24,27,28,29,31,32,35,36,37,40,42}
)

for host in "${hosts[@]}"; do
  echo "host: ${host}"
  #ssh root@$host uptime
  ssh "root@${host}" ip -br a | grep "10.38.140."
  echo ____________________________________________________________
done

