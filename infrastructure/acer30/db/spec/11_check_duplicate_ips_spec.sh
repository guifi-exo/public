#!/bin/sh

Describe "Validate network.yml"
  Describe "check_duplicate_ips()"
    Include ./validate_network.sh
    It "does not fail without duplicates"
      Data
        #|vms_exo:
	#|  - vlan100:
	#|      ip: 192.168.0.1/24
	#|      ipmi: 192.168.1.1/24
	#|  - vlan100:
	#|      ip: 192.168.0.2
	#|      ip6: "2a0a::1"
        #|vms_3parties:
	#|  - vlan100:
	#|      ip: 192.168.0.3/24
	#|      ip6: "2a0a::77/48"
        #|custom_allocated_servers:
	#|  - vlan100:
	#|      ip: 192.168.0.4/22
	#|      ip6: "2a0a::78/64"
      End
      When run check_duplicate_ips
      The status should be success
      The stdout should be blank
      The stderr should be blank
    End

    It "fails and lists duplicates"
      Data
        #|vms_exo:
	#|  - vlan100:
	#|      ip: 192.168.0.1/24
	#|      ipmi: 192.168.0.1/24
	#|  - vlan100:
	#|      ip: 192.168.0.2
	#|      ip6: "2a0a::1"
        #|vms_3parties:
	#|  - vlan100:
	#|      ip: 192.168.0.3/24
	#|      ip6: "2a0a::77/48"
        #|custom_allocated_servers:
	#|  - vlan100:
	#|      ip: 192.168.0.4/22
	#|      ip6: "2a0a::77/64"
      End
      When run check_duplicate_ips
      The status should be failure
      The stdout should be blank
      The stderr should equal "\
192.168.0.1
2a0a::77"
    End
  End
End
