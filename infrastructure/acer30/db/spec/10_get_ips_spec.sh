#!/bin/sh

Describe "Validate network.yml"
  Describe "get_ips()"
    Include ./validate_network.sh
    It "returns exactly what is needed"
      Data
        #|random_stuff:
	#|  some_entry: hello
        #|vms_exo:
	#|  - hostname: host
	#|    system: something something
	#|    vlan100:
	#|      ip: 192.168.0.1/24
	#|      ipmi: 192.168.1.1/24
	#|    comment: More random stuff
	#|  - hostname: host2
	#|    system: something something
	#|    vlan100:
	#|      ip: 192.168.0.2
	#|      ip6: "2a0a::1"
	#|    comment: More random stuff
        #|vms_3parties:
	#|  - hostname: host3
	#|    system: something something
	#|    vlan100:
	#|      ip: 192.168.0.3/24
	#|      ip6: "2a0a::77"
	#|    comment: More random stuff
        #|custom_allocated_servers:
	#|  - hostname: host4
	#|    system: something something
	#|    vlan100:
	#|      ip: 192.168.0.4/22
	#|    comment: More random stuff
        #|unnecessary_thing:
	#|  - hostname: host5
	#|    system: something something
	#|    vlan100:
	#|      ip: 192.168.0.5/24
	#|      ip6: "2a0a::5"
	#|    comment: More random stuff
      End
      When run get_ips
      The status should be success
      The stdout should equal "\
192.168.0.1/24
192.168.1.1/24
192.168.0.2
2a0a::1
192.168.0.3/24
2a0a::77
192.168.0.4/22"
      The stderr should be blank
    End
  End
End
