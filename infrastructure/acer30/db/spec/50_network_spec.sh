#!/bin/sh

Describe "Validate network.yml"
  Include ./validate_network.sh
  It "contains valid YAML"
    Data < "./network.yml"
    When run is_valid_yaml
    The status should be success
    The stdout should be present
    The stderr should be blank
  End
End
