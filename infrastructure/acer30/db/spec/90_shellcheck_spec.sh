#!/bin/sh

Describe "shellcheck compliance"
  It "is valid"
    When run find . -type f -name '*.sh' -exec shellcheck -a '{}' '+'
    The status should be success
    The stderr should be blank 
    The stdout should be blank
  End
End
