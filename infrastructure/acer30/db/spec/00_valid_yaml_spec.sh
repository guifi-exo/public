#!/bin/sh

Describe "Validate network.yml"
  Describe "is_valid_yaml()"
    Include ./validate_network.sh
    It "fails on invalid YAML"
      Data
        #|- ripe_network_prefixes
        #|  - title: test1
        #|    prefix: 2a0a:fg::1
      End
      When run is_valid_yaml
      The status should be failure
      The stderr should be present
      The stdout should be blank
    End

    It "passes on valid YAML"
      Data
        #|- ripe_network_prefixes:
        #|  - title: test1
        #|    prefix: "2a0a:fg::1"
      End
      When run is_valid_yaml 
      The status should be success
      The stdout should be present
      The stderr should be blank
    End
  End
End
