#!/bin/sh

Describe "guifi-exo/public"
  It "is up to date"
    work_offline() {
      test -n "${WORK_OFFLINE}"
    }
    # Update when not working offline
    if ! work_offline; then git fetch -q; fi
    Skip if "Working offline" work_offline
    When run git rev-parse HEAD
    The status should be success
    The stderr should be blank
    The stdout should eq "$(work_offline || git rev-parse '@{u}')"
  End
End
