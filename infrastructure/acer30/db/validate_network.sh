#!/bin/sh -e

is_valid_yaml() {
	yq .
	return $?
}

get_ips() {
	yq -r '.["vms_exo","vms_3parties","custom_allocated_servers"] | .[] | to_entries[] | select(.key | startswith("vlan")) | [getpath(["value", "ip"], ["value", "ipmi"], ["value", "ip6"])] | flatten[] | select(. != null)'
}

get_vms_param() {
    yq -r '.["vms_exo","vms_3parties"] | .[] | to_entries[] | select(.key | startswith("'"$1"'")).value'
}

check_duplicate_ips() {
	ips="$(get_ips | cut -d '/' -f 1 | sort | uniq -d)"
	if [ -n "${ips}" ]; then
		echo "${ips}" >> /dev/stderr
		return 1
	else
		return 0
	fi
}

check_valid_ips() {
	for ip in $(get_ips); do
		if sipcalc "${ip}" | grep -q 'ERR'; then
			echo "[INVALID]	${ip}" >> /dev/stderr
			failed="YES"
		fi
		if ! echo "${ip}" | grep -q '/'; then
			echo "[NOSUBNET]	${ip}" >> /dev/stderr
			failed="YES"
		fi
	done
	if [ -n "${failed}" ]; then
		return 1
	else
		return 0
	fi
}

check_duplicate_vmis() {
	ips="$(get_vms_param "vmid" | sort | uniq -d)"
	if [ -n "${ips}" ]; then
		echo "${ips}" >> /dev/stderr
		return 1
	else
		return 0
	fi
}

check_valid_vmis() {
    for vmid in $(get_vms_param "vmid"); do
        if ! [ "$vmid" -gt 0 ] 2> /dev/null; then
            echo "${vmid}" >> /dev/stderr
            failed="YES"
        fi
    done
	if [ -n "${failed}" ]; then
		return 1
	else
		return 0
	fi
}

check_duplicate_hostnames() {
	hostnames="$(get_vms_param "hostname" | sort | uniq -d)"
	if [ -n "${hostnames}" ]; then
		echo "${hostnames}" >> /dev/stderr
		return 1
	else
		return 0
	fi
}


case $1 in
	--file|-f)
		file="$2"
		echo "Validating YAML..." >> /dev/stderr
		is_valid_yaml > /dev/null < "${file}"

		echo "Checking duplicate IPs..." >> /dev/stderr
		check_duplicate_ips > /dev/null < "${file}"

		echo "Checking IP validity..." >> /dev/stderr
		check_valid_ips > /dev/null < "${file}"

		echo "Checking duplicate VMIDs..." >> /dev/stderr
		check_duplicate_vmis > /dev/null < "${file}"

		echo "Checking VMID validity..." >> /dev/stderr
		check_valid_vmis > /dev/null < "${file}"

		echo "Checking duplicate hostnames..." >> /dev/stderr
		check_duplicate_hostnames > /dev/null < "${file}"
	;;
esac
