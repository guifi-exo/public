# WORK_OFFLINE var

If you want do to some commits offline, you need to do `WORK_OFFLINE='y' git commit (...)` to skip the last commit that checks if your latest commit was sync with remote git repo

# Testing

If you just want to test your `network.yml`, run:

    ./validate_network.sh --file network.yml

This depends on:
- `yq(1)`: which can be installed with `pip` or `pipenv`
- `jq(1)`: which can be installed with your package manager
- `sipcalc(1)`: which can be installed with your package manager

# CI / test suite

This is using [ShellSpec](https://shellspec.info) to run the tests and to
describe how the shell scripts should behave.
We recommend installing it using the manual mode from git:
https://github.com/shellspec/shellspec#others-archive--make--manual

Shell code is also tested using [ShellCheck](https://www.shellcheck.net) to
ensure best practises.
You can install it with your package manager.

# View

If you want to visualize all IPs used in `network.yml`, run:

    ./show_ips.sh

there are two optional parameters: file and vlan
