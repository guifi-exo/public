#!/bin/sh -e

# another option (?)
# cat network.yml | yq '.["vms_exo"] | .. | .vlan94?.ip?' | grep -v 'null' | sort
get_ips() {
	yq -r '.["vms_exo","vms_3parties","custom_allocated_servers"] | .[] | to_entries[] | select(.key | startswith("vlan'"${1}"'")) | [getpath(["value", "ip"], ["value", "ipmi"], ["value", "ip6"], ["value", "floating_ip"], ["value", "floating_ip6"])] | flatten[] | select(. != null)'
}

# defaults
file="network.yml"

# optional args
while [ $# -gt 0 ]; do
	case "${1}" in
		--file|-f)
			file="$2"
			shift
			shift
		;;
		--vlan)
			vlan="$2"
			shift
			shift
		;;
		*)
			echo "Unrecognized option ${1}"
			exit 1
	esac
done

# sort ipv6
get_ips "${vlan}" < "${file}" | cut -d '/' -f 1 | sort -t : -k 3,3d -k 4,4d -k 5,5n -k 6,6n | grep "^2a"

# sort ipv4 -> src https://www.cyberciti.biz/faq/unix-linux-shell-script-sorting-ip-addresses/
get_ips "${vlan}" < "${file}" | cut -d '/' -f 1 | sort -t . -k 3,3n -k 4,4n | grep -v "^2a"
