#!/bin/bash

scp exo-core:/etc/network/interfaces core/etc/network/
scp exo-core:/etc/systemd/system/bind9.service core/etc/systemd/system/
scp -r exo-core:/etc/bind/ core/etc/
scp exo-core:/etc/firewall4 core/etc/
scp exo-core:/etc/firewall6 core/etc/
scp exo-core:/etc/ntp.conf core/etc/

scp exo-rproxy:/etc/network/interfaces rproxy/etc/network/
scp exo-rproxy:/etc/haproxy/haproxy.cfg rproxy/etc/haproxy/
scp exo-rproxy:/etc/firewall4 rproxy/etc/
scp exo-rproxy:/etc/firewall6 rproxy/etc/
scp -r exo-rproxy:/etc/nginx/sites-enabled rproxy/etc/nginx
scp -r exo-rproxy:/etc/default/sslh/ rproxy/etc/default/

scp exo-trax1:/etc/network/interfaces trax1/etc/network
scp exo-trax1:/etc/firewall4 trax1/etc/
scp exo-trax2:/etc/network/interfaces trax2/etc/network
scp exo-trax2:/etc/firewall4 trax2/etc/
scp exo-trax3:/etc/network/interfaces trax2/etc/network
scp exo-trax3:/etc/firewall4 trax3/etc/
