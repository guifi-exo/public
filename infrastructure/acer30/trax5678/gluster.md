Thanks ramon selga for sharing your knowledge and patience

Our new gluster `vm_nvme` volume is replica 3 (2 disks can fail and it still works), as we have 4 nodes (with 2 disks each one; a total of 8), but we need to have a multiple of three. Ramon suggested to do a *stripped pool* (which behaves similar to a *raid 0*; increase capacity of disks with no redundancy). That strategy let us to reduce the number of disks so when we multiply it by 3, we are close to ranges between 10 to 20 which are good for gluster performance. Loosing one disk means loosing two, but replica 3 plus NVMe disk reduces the probability of an incident.

## zfs raid0

```sh
# get devices of max and equal size

maxsize=$(lsblk -o SIZE -x SIZE | tail -n 1)

targetdisks=$(lsblk -o NAME,SIZE,TYPE -x SIZE \
  | grep "$maxsize" | grep disk | awk '{print $1}')

vdev=$(ls -l /dev/disk/by-id/nvme-eui* \
  | grep -v part | grep -E $(echo $targetdisks | tr ' ' '|') \
  | cut -d "/" -f 5 | cut -d " " -f 1)

zpool create -o ashift=12 \
        -O acltype=posixacl -O compression=lz4 \
        -O relatime=on -O xattr=sa \
        vmpool $vdev

totalMB=$(df -m | grep /vmpool | awk '{print $2}')

# zfs raid0 of n devices of same size and 3 bricks per node
nbricks=3

brickquota=$(($totalMB/$nbricks/1024))

for i in $(seq 1 $nbricks); do
  zfs create vmpool/brick$i
  zfs set quota=${brickquota}G vmpool/brick$i
done
```

initialize a replica 3 gluster

```sh
gluster volume create vm_nvme replica 3 \
  gfs5:/vmpool/brick1/vm_nvme gfs6:/vmpool/brick1/vm_nvme gfs7:/vmpool/brick1/vm_nvme \
  gfs8:/vmpool/brick1/vm_nvme gfs5:/vmpool/brick2/vm_nvme gfs6:/vmpool/brick2/vm_nvme \
  gfs7:/vmpool/brick2/vm_nvme gfs8:/vmpool/brick2/vm_nvme gfs5:/vmpool/brick3/vm_nvme \
  gfs6:/vmpool/brick3/vm_nvme gfs7:/vmpool/brick3/vm_nvme gfs8:/vmpool/brick3/vm_nvme
```

a script that tries to generalize the gluster brick situations

```sh
replica=3
nbricks=3
oid=4         # offset gluster ID
nodes=4
bricks="$(
for b in $(seq 1 $nbricks); do
  # your gluster IDs
  for g in $(seq $((1 + oid)) $((nodes + oid))); do
    printf "gfs${g}:/vmpool/brick${b}/vm_nvme "
    counter=$((counter + 1))
    if [ $((counter % replica)) -eq 0 ] &&
      [ $counter -ne $((nbricks * nodes)) ]; then
      echo '\'
    fi
  done
done
)"

gluster volume create vm_nvme replica 3 "$bricks"
```

we take the variables from `/var/lib/glusterd/groups/virt` but increase the shard block to 512MB, we apply it with this script

```
gluster_features=$(cat <<EOF
performance.quick-read=off
performance.read-ahead=off
performance.io-cache=off
performance.low-prio-threads=32
network.remote-dio=enable
cluster.eager-lock=enable
cluster.quorum-type=auto
cluster.server-quorum-type=server
cluster.data-self-heal-algorithm=full
cluster.locking-scheme=granular
cluster.shd-max-threads=8
cluster.shd-wait-qlength=10000
features.shard=on
features.shard-block-size=512MB
user.cifs=off
cluster.choose-local=off
client.event-threads=4
server.event-threads=4
performance.client-io-threads=on
EOF
)
# just read
for i in $gluster_features; do echo gluster volume set vm_nvme $(echo $i | tr '=' ' '); done
# apply
for i in $gluster_features; do gluster volume set vm_nvme $(echo $i | tr '=' ' '); done
```



check everything is fine `gluster volume info vm_nvme` and start gluster volume `gluster volume start vm_nvme`

```sh
for i in $(seq 1 7); do
  ssh trax$i 'mkdir /vm_nvme'
  ssh trax$i 'echo -e "gfs:vm_nvme\t/vm_nvme\tglusterfs\tdefaults,_netdev,noauto,x-systemd.automount\t0\t0" >> /etc/fstab'
done
```

check fstab state `for i in $(seq 1 8); do ssh trax$i 'cat /etc/fstab'; done`

mount new gluster in all nodes `for i in $(seq 1 8); do ssh trax$i 'mount /vm_nvme'; done`

```
gluster v set vm_nvme network.ping-timeout 16
gluster v set vm_nvme server.tcp-user-timeout 16
```
