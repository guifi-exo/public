gluster v info

```
Volume Name: store
Type: Distributed-Replicate
Volume ID: 62d20f5d-2007-4c8b-b9eb-dc5d60f37052
Status: Started
Snapshot Count: 0
Number of Bricks: 2 x (2 + 1) = 6
Transport-type: tcp
Bricks:
Brick1: gfs1:/brick2/store
Brick2: gfs3:/brick2.1/store
Brick3: gfs2:/brick2.a/store (arbiter)
Brick4: gfs2:/brick2/store
Brick5: gfs3:/brick2.2/store
Brick6: gfs1:/brick2.a/store (arbiter)
Options Reconfigured:
network.ping-timeout: 5
cluster.shd-wait-qlength: 8192
cluster.shd-max-threads: 2
user.cifs: off
cluster.locking-scheme: granular
cluster.server-quorum-type: server
cluster.quorum-type: auto
cluster.eager-lock: enable
performance.read-ahead: off
network.remote-dio: on
cluster.data-self-heal-algorithm: full
performance.stat-prefetch: off
performance.io-cache: off
performance.quick-read: off
performance.readdir-ahead: off
features.shard: enable
features.shard-block-size: 256MB
transport.address-family: inet
nfs.disable: on

Volume Name: vmstore
Type: Replicate
Volume ID: 8094bac4-9d16-454e-86c1-0cc8741a65aa
Status: Started
Snapshot Count: 0
Number of Bricks: 1 x (2 + 1) = 3
Transport-type: tcp
Bricks:
Brick1: gfs1:/brick1/vmstore
Brick2: gfs2:/brick1/vmstore
Brick3: gfs3:/brick1/vmstore (arbiter)
Options Reconfigured:
network.ping-timeout: 5
cluster.shd-wait-qlength: 8192
cluster.shd-max-threads: 2
user.cifs: off
cluster.locking-scheme: granular
cluster.eager-lock: enable
network.remote-dio: on
cluster.quorum-type: auto
cluster.server-quorum-type: server
cluster.data-self-heal-algorithm: full
features.shard-block-size: 64MB
features.shard: enable
performance.stat-prefetch: off
performance.io-cache: off
cluster.readdir-optimize: on
performance.read-ahead: off
performance.quick-read: off
transport.address-family: inet
performance.readdir-ahead: off
nfs.disable: on
```
