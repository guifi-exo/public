Pla d'igualtat de gènere 2020,
per portar a l'Assemblea General de 2020 de l'eXO
Associació expansió de la Xarxa Oberta (exo.cat)

# Pla d'Igualtat de gènere 2020, eXO

## Diagnòstic i Accions al 2020 per llençar un Pla complert el 2021

### Introducció

Associació expansió de la Xarxa Oberta / eXO: és una associació sense ànim de
lucre i un operador de telecomunicacions que centra la seva activitat en el
marc de l'economia social i solidària. 
El seu objectiu principal és la difusió i expansió de Guifi.net, 
una xarxa de telecomunicacions de comuns i una eina contra l'exclusió digital. 
L'entitat duu a terme trobades setmanals conegudes com els guifilabs, 
per tal de posar en comú coneixements, idees i projectes. 
També desenvolupa activitats formatives, divulgatives, de manteniment, 
ampliació i operació de la xarxa Guifi.net del Barcelonés. 
La eXO / exo.cat: ofereix serveis de cobertura Wifi per a esdeveniments socials
i provisió d'accés a Internet.

L'assemblea de sòcis/sòcies es el màxim òrgan de governança. 
Un cop a l'any en l'Assemblea ordinària es ratifiquen o s'anomenen els 
càrrecs que conformen la Junta de l'associació. 
Aquests càrrecs sempre han estat ocupats per persones de gènere masculí.

L'associació té un nombre total d'associats/associades de **97** de les 
quals **6** son jurídiques i només **13** són dones.

Les persones membres més activistes i que disposen de temps
es reuneixen un cop a la setmana en els anomenats "guifilabs"
en els que entre activitats tecnològiques es comenten aspectes associatius
i es fan propostes que afecten a l'associació i que es traslladen a la Junta.
Altres propostes es presenten a les assemblees ordinàries o extraordinàries,
per la seva discusió i/o execució.

Les eines de comunicació interna són a part dels guifilabs,
la llista de correu: **exo-membres@llistes.exo.cat**
i els serveis de sales de converses Riot: matrix.guifi.net.
Cada cop més, aquest espai de sales de converses te una presència
més important pel debat intern i les propostes.
Està guanyant importància també, per a la documentació i elaboració
col.laborativa de informació i discusió,
la wiki de la eXO a https://gitlab.com/guifi-exo/wiki

La varietat de gènere en aquestes activitats i eines de comunicació internes
i externes de l'associació és pràcticament nula.

En quant a la comunicació externa: esporàdicament s'han generat tríptics,
flyers, pegatines i algún altre folletó i pancarta.
El referent de comunicació externa es la Web de l'associació: exo.cat.
No tenim un pla de comunicació interna/externa de l'associació.

## Accions del pla pel 2020:
    
### a) Elaborar un diagnòstic:

A part de valorar la influència de una comunicació externa deficitària:
caldrà definir com enfoquem aquest tema de cara al 2021.
Es proposa enfocar desde dues perspectives en la
elaboració del diagnòstic (amb el símptoma evident de la presència
pràcticament nula de dones, en les activitats de l'associació):
una perspectiva desde "Tecnologia i gènere" i l'altre desde "Activisme i gènere".

Aquest diagnòstic es basarà incialment en les conclusions que siguem
capaços d'extreure de entrevistes realitzades per les
pròpies persones associades, així com en altres fonts:
textos i publicacions. Caldrà definir per el proper Pla del 2021
la necessitat o no de comptar amb assesorament extern per fer
un diagnòsic més acurat. Es proposen un mínim de 8 (2 per trimestre)
entrevistes llistades a continuació.

#### a1) Tecnologia i gènere
- Entrevistes a dones (annex 01) activistes de guifi.net
- Entrevistes en el món acadèmic
- Entrevistes en el món professional (TIC publiques, privades)
- Entrevistes a entitats més semblants a la nostra, associacions,...
- Publicacions (recollir i exposar a l'associació i treure conclusions)

Elaborar les preguntes o temes a tractar en les entrevistes (4-5 preguntes).
Si es disposa d'assesorament millor sino a discreció de la persona entrevistadora.

#### a2) Activisme i associacionisme i gènere
- Demanar opinions, en una primera fase, més informals (no cal format d'enquesta/entrevista) a dones que coneguem siguin o no de la eXO.
- Explorar experiències d'altres entitats, es poden incloure en les enquestes anteriors, associacions d'àmbit TIC.
- Entrevistar altres entitats no tecnològiques
- Visió de l'activisme desde la varietat de gènere.

D'aqui ha de sortir una proposta d'estudi en quant a millorar el
diagnòstic pel 2021. Caldria proposar una tutoria de
fora de l'associació pel diagnòstic.

### b) Realitzar activitats pel 2020 directament relacionades amb la igualtat de gènere:

#### b1) Tallers
- L'associació es compromet a realitzar un taller o xerrada per trimestre (2 abans i 2 desprès d'estiu).
- Utilitzant els guifilab o buscant una data especifica.
- Els tallers han de tenir en compte els aspectes de: tecnologia i gènere, i/o activisme i gènere.
- Caldrà buscar persones o grups convidades: Entre altres entitats amb més expertesa que la nostre associació en igualtat de gènere.
- S'intentarà fer una avaluació del taller per que pugui constar en la memòria i avaluació del Pla del 2020

#### b2) Altres propostes:
- Els serveis que entrega la eXO als seus associats formen part sovint de serveis que s'utilitzen per unitats familiars, unitats d'habitatge, entitats. Definir durant el 2020 si es pot fer una mena de membresìa de associats de cara a tenir en compte aquest potencial, no sols quantitatiu sino també de cara al disseny dels propis serveis.
- Tractar altres temes fonamentals com "llenguatge no sexista", etc.

## Avaluació/indicadors/mesura de l'execució del Pla durant el 2020

Aquest primer any ho fem simple: llistat de: entrevistes proposades,
documents aportats, tallers i altres propostes amb indicadors: SI/NO i/o % realitzat.

Els/les participants en la Sala de comunicació de Riot "Pla-genere"
elaborarem conjuntament un informe trimestral per a la Junta sobre
la base d'aquest pla, aquesta ens ho exigirà si hi ha retard.

El informe final anirà acompanyat del Pla pel 2021, quan ho tinguem més madur ho podem fer bianual o trianual.

Es convenient adoptar una auditoria/revisió externa per aquest
i sobre tot pel proper pla. Si som capaços de fer-ho en aquest any millor,
però no es mesurable en aquest pla.

## Annexes del Pla d'igualtat de gènere

Aquets annexes els hem de crear en la wiki de la eXO, per anar-los treballant i omplint durant l'any.
Afegeixo el que ha anat sortint sobre els annexos, però hauran de tenir document propi,
enllaçat des d'aquest pla.

### [Annex 00] Preparar un històric de les accions ja realitzades en relació al contingut del Pla d'igualtat de gènere en altres anys.

 - Anem recollint amb un breu resum:
 - Llista d'actes realitzats per la eXO i valorant presencia variada de genere:
 - - Treball de fi de carrera sobre guifi-net presentat a Espai30
 - - La trobada amb pyladies
 - - ...

 * Sobre les entrevistes: aquest any no cal que tinguem tots el models de preguntes o de com enfocar el tema, es tasca durant el 2020, ho podem obtenir de les mateixes persones entrevistades, Sobre el nombre mínim de entrevistes *

### [Annex 01] Preguntes per entrevistes a col.laboradores de guifi.net

 - 1 Trajectòria professional
 - 2 Com vas conèixer guifi.net
 - 3 Que es per a tu guifi.net
 - 4 Posar la ultima pregunta, de forma genèrica, de l'autoentrevista de Adriana
 - 5 Afegir les autopreguntes que proposin les persones entrevistades!

### [Annex 99] Exemple de fitxa d'avaluació continua del Pla (de mínims)

- Entrevistes a col.laboradores de guifi.net ..............................[ 50 %]
- - Adriana, URL del blog de la eXO (Feta, pendent de posar a la web)
- - Proposar qui i quan, ja tenim el com: model preguntes Adriana
- Entrevistes a personal del mon academic ...................................[  %]
- - Proposar qui, quan i com
- Entrevistes en l'entorn públic o privat empresarial de les TIC ............[  %]
- - Proposar qui, quan i com
- Entrevistes en el mon associatiu, cooperatiu, etc .........................[  %]
- - Proposar qui, quan i com
- Publicacions ..............................................................[  %]
- - Llista de docs "revisats"
- Entrevistes sobre activisme i genere ......................................[  %]
- - Proposar qui, quan i com

- Proposta d'estudi pel 2021: Visió de l'activisme desde la varietat de gènere.
- El que hem aprés i quin cami podem recorrer ...............................[  %]
- - Proposar contingut

- Tallers realitzats ........................................................[  %]

- - Proposar qui, quan i com. També ens podem animar a fer més, però fem un compromis de mínims.

- Col.laboracions ...........................................................[  %]
- - Proposar qui, quan i com

## Altres

Podem afegir possibles entrevistes, material documental, propostes de taller, etc. a **https://gitlab.com/guifi-exo/wiki/-/tree/master/info/genere**

Proposta Efraim: Taller sobre lenguatge no sexista (demanar a l'ajuntament)
