Organització administrativa de la eXO

# LDAP

La eXO farà anar LDAP com a base de dades de gestió de usuaris i control d'accés.

Hi han processos manuals i automàtics.

Esquema:

- grups (posixGroup): nivells de permisos, visibilitat, es van definir pel servei nextcloud però és generalitzable
  - gestió: administrar l'associació (gidNumber 30000)
  - associació: visibilitat per socis de l'associació, transparència, documents (gidNumber 30001)
  - public: allò que tothom pot veure o fer anar (gidNumber 30002)
- serveis (groupOfNames): aplicacions que volem donar accés
- serveis-autenticacio: les aplicacions necessiten connectar-se a LDAP, en aquest subarbre ho fan de forma que només tenen permisos de **lectura** perquè només han d'autoritzar usuaris
- serveis-administracio: determinades aplicacions tals com nextcloud es connecten al LDAP i tenen permisos d'**escriptura**, de moment creiem suficient que només puguin canviar la contrassenya
- únic servei amb permisos administració total: dolibarr (no cal que sigui un grup)
- usuaris: identitats (persones físiques o jurídiques) donades d'alta a l'associació o clients actius
  - *si hi ha un camp emplenat (quin?) en els usuaris vol dir que és un client*

Funcions:

- mateix usuari i contrassenya per la mateixa identitat i entre els diferents serveis compatibles amb connector LDAP
- facilitar procés d'alta i baixa d'identitats

## procediment manual

el procediment manual implica fer anar eines tècniques ldap tals com Apache Directory Studio o ldapvi. Les tasques serien:

- creació de nous grups i serveis
- gestió de membresia d'usuaris i la seva membresia als diferents ou=grups i ou=serveis

## dolibarr

Dolibarr és una base de dades de persones relacionades amb l'associació:

- Mòdul *Socis* gestiona les dades personals dels socis i clients que poden ser persones físiques o jurídiques
  - truc: per distingir socis de clients (i plasmar-ho en LDAP també) hi haurà un camp (descripció o nota) que es renombrarà perquè sigui *Client*, que es propagarà cap a LDAP també i que si té contingut vol dir que aquell usuari és un client
- Mòdul *Tercers* gestiona les factures, que poden provenir de socis o clients externs

Donar d'alta:

- En cas de ser un soci, via dolibarr, la propagació és `dolibarr -> LDAP` i la sincronia és automàtica
- En cas de ser un no-soci, via procediment manual ldap. S'entén que no és preocupant perquè haurien de ser casos poc habituals

## nextcloud

Nextcloud és la nova aplicació en substitució de dropbox.com que s'ha anat fent servir per la documentació de l'associació (a part de l'eina complementària guifi-exo/wiki / gitlab)

Tot usuari donat d'alta en LDAP ha de poder entrar a nextcloud (sense fitxers compartits, sense quota) perquè puguin modificar la seva pròpia contrassenya:

- canviar contrassenya
- recordar contrassenya via email

Hi haurà tres carpetes amb els permissos de ou=grups de LDAP:

- gestió: visibilitat a persones i entitats que col·laboren en la gestió de l'associació, documents sensibles i relacionats amb aquest propòsit
- associació: visibilitat per socis (no clients) de l'associació, documents sensibles per ser públics però que han d'estar a l'abast dels socis
- public: allò que tothom pot veure
